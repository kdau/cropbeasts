namespace Cropbeasts.Beasts
{
	public class QiBeast : Berrybeast
	{
		public QiBeast ()
		{ }

		public QiBeast (CropTile cropTile, bool primary)
		: base (cropTile, primary, "Qi Beast")
		{
			// The harvest already has a face.
			faceType.Value = FaceType.Blank;
		}
	}
}
