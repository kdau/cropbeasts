# Release notes

[Cropbeasts](../)

## 2.0.1

*Release pending.*

* Fix error message on new save creation

## 2.0.0

*Released 2021 June 28.*

* Require, and fix compatibility with, Stardew 1.5 or higher
* Support new crops for 1.5
* Support a new farm area for 1.5
* Add configurable daily chance of any cropbeasts (thanks to SansModowanie for suggestion)
* Adjust minimum crops in a location for cropbeast spawning
* Fix rare audio errors for Leafbeasts
* Replace `revert_cropbeasts` console command with broader `reset_cropbeasts`
* Move Json Assets pack to separate directory

## 1.0.1

*Released 2020 June 8.*

* Fix rare slowdowns due to sound timing problem
* Fix blank name and description for Beast Hat in some languages
* Exempt special giant crops on IF2R from spawning as cropbeasts
* Streamline documentation

## 1.0.0

*Released 2020 May 10.*

* Initial production release
* Changes since last beta:
	* Add "Tamer of Beasts" achievement for slaying every type
	* Add "Beast Mask" hat unlocked with achievement
	* Add documentation on cropbeast types
	* Fix spawn rate calculations

## 0.3.1

*Released 2020 May 4.*

* Fix regression in 0.3.0 that prevented automatic spawning

## 0.3.0

*Released 2020 May 4.*

* Finish multiplayer compatibility

## 0.2.0

*Released 2020 April 27.*

* Add "High Contrast" option for higher visibility
* Add "Tracking Arrows" option for easier tracking
* Make screen effect from Cactusbeast sandblast attack optional
* Improve rendering of sandblast effect, and heal from it quickly when swimming
* Make Rootbeast hiding behavior optional
* Don't bias towards choosing a certain beast more than once a day
* Preliminary work towards multiplayer compatibility

## 0.1.0

*Released 2020 April 21.*

* Limited-release beta version
